package main

import (
	"log"
	"os"

	"gitlab.com/jannickfahlbusch/partnerchannel"
)

var (
	err                  error
	partnerChannelClient *partnerchannel.Client
)

func main() {
	parameterList := os.Args[1:]

	// Exit if nothing was provided
	if len(parameterList) == 0 {
		os.Exit(1)
	}

	action := parameterList[0]
	hostname := parameterList[1]
	fileName := parameterList[2]
	tokenValue := parameterList[3]

	switch action {
	case "challenge-dns-start":
		startChallenge()
	case "challenge-dns-stop":
		stopChallenge()
	default:
		os.Exit(42)
	}

}

func login() error {
	partnerChannelClient, err := partnerchannel.NewClient(os.Getenv("PARTNERCHANNEL_API_KEY"), nil)
	if err != nil {
		return err
	}

	_, _, err = partnerChannelClient.Account.Login(&partnerchannel.LoginRequest{
		UserName:     os.Getenv("PARTNERCHANNEL_USERNAME"),
		Password:     os.Getenv("PARTNERCHANNEL_PASSWORD"),
		ResellerName: os.Getenv("PARTNERCHANNEL_RESELLERNAME"),
	}, nil)

	return err
}

func startChallenge() {
	err = login()
	if err != nil {
		log.Fatal(err)
	}

	partnerChannelClient.Acme.AddChallenge()
}

func stopChallenge() {
	err = login()
	if err != nil {
		log.Fatal(err)
	}

	partnerChannelClient.Acme.RemoveChallenge()
}
